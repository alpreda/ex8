package com.orangetraining;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // Create a list called MyHotelEmployees
        List<String> MyHotelEmployes = new ArrayList<String>();

        //Add some elements to that list
        MyHotelEmployes.add( "Will Smith" );
        MyHotelEmployes.add( "Benjamin McCartney" );
        MyHotelEmployes.add( "Albert Davidson" );
        MyHotelEmployes.add( "Jaden Clinton" );
        MyHotelEmployes.add( "Donald Lincoln" );
        MyHotelEmployes.add( "Bart Bureau" );
        MyHotelEmployes.add( "Alistaire O'Doherty" );

        //Print size and the employees name
        System.out.println( "The number of employees of the hotel is: " + MyHotelEmployes.size() + "." );
        System.out.println( "Their name are: " + MyHotelEmployes.toString() + "." );

        //Remove employee at the 1st floor
        System.out.println( "The designated employee on the first floor was fired for innappropiate behaviour" );
        MyHotelEmployes.remove(1);

        //Print the new list of employees
        System.out.println(MyHotelEmployes.toString());
        System.out.println( "The new number of employees of the hotel is: " + MyHotelEmployes.size() + "." );

        //Print all employees using for loop
        for(String employee:MyHotelEmployes){
            System.out.println(employee);
        }

        //Replacing the old employee using set() method
        System.out.println( "The new substitute will be Steve Irwin" );
        MyHotelEmployes.set( 1, "Steve Irwin" );

        //Print the new list of employees
        System.out.println( "The final list of employees is : " + MyHotelEmployes.toString() + "." );
    }
}





